import re
from datetime import date, timedelta

import requests
from django.contrib.auth.models import Group, User
from django.http import HttpResponse, JsonResponse
from django.utils.html import strip_tags
from rest_framework import permissions, viewsets, generics
from django_filters.rest_framework import DjangoFilterBackend

from imago.models import (DailyPray, Examen, ExamenType, Igenaptar, Language,
                          MonthlyPray, ExamenStep, ExamenMusic)
from imago.serializers import (DailyPraySerializer, ExamenSerializer,
                               ExamenTypeSerializer, GroupSerializer,
                               MonthlyPraySerializer, UserSerializer,
                               ExamenStepSerializer, ExamenMusicSerializer)


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [permissions.IsAuthenticated]


class DailyPrayViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows DailyPray to be viewed or edited.
    """
    queryset = DailyPray.objects.all()
    serializer_class = DailyPraySerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['pray_date', 'title']


class MonthlyPrayViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows  MonthlyPray to be viewed or edited.
    """
    queryset = MonthlyPray.objects.all()
    serializer_class = MonthlyPraySerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['pray_date', 'title']


class ExamenViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows ExamenView to be viewed or edited.
    """
    queryset = Examen.objects.all()
    serializer_class = ExamenSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['examen_type']


class ExamenTypeViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows ExamenType to be viewed or edited.
    """
    queryset = ExamenType.objects.all()
    serializer_class = ExamenTypeSerializer


class ExamenStepViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows ExamenMusic to be viewed or edited.
    """
    queryset = ExamenStep.objects.all()
    serializer_class = ExamenStepSerializer


class ExamenMusicViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows ExamenMusic to be viewed or edited.
    """
    queryset = ExamenMusic.objects.all()
    serializer_class = ExamenMusicSerializer


class DailyPayFilter(generics.ListAPIView):
    serializer_class = DailyPraySerializer

    def get_queryset(self):
        day = self.kwargs['day']
        if day is None:
            day = date.today().strftime("%Y-%m-%d")

        return DailyPray.objects.filter(pray_date=day)


class MonthlyPrayFilter(generics.ListAPIView):
    serializer_class = MonthlyPraySerializer

    def get_queryset(self):
        day = self.kwargs['day']
        if day is None:
            day = date.today().strftime("%Y-%m-%d")

        return MonthlyPray.objects.filter(pray_date=day)


def zoli(request):
    response = "Hello Zoli"
    return HttpResponse(response)


def daterange(start_date, end_date):
    for n in range(int((end_date - start_date).days)+1):
        yield start_date + timedelta(n)


def loop_year_fill_igenaptar_content(request, year=None):
    if year is None:
        year = date.today().year

    start_date = date(year, 1, 1)
    end_date = date(year, 12, 31)
    for single_date in daterange(start_date, end_date):
        # print(single_date.strftime("%Y-%m-%d"))
        fill_igenaptar_conent(single_date)

    return HttpResponse("Done")


def loop_year_parse_igenaptar(request, year=None):
    if year is None:
        year = date.today().year

    start_date = date(year, 1, 1)
    end_date = date(year, 12, 31)
    for single_date in daterange(start_date, end_date):
        # print(single_date.strftime("%Y-%m-%d"))
        igenaptar_content(dict(), single_date.strftime("%Y-%m-%d"))

    return HttpResponse("Done")


def fill_igenaptar_conent(day):
    r = requests.get(f'https://igenaptar.katolikus.hu/nap/index.php?holnap={day.strftime("%Y-%m-%d")}')
    # time = timezone.now() + datetime.timedelta(days=30)
    i = Igenaptar.objects.filter(pray_date=day).first()
    if i is None:
        i = Igenaptar()
    i.content = r.text
    i.pray_date = day
    i.lang = Language.HU
    i.save()


def dailypay(request, day=None):
    if day is None:
        day = date.today().strftime("%Y-%m-%d")
    i = DailyPray.objects.filter(pray_date=day).first()
    d = dict()
    d["title"] = i.title
    d["content"] = i.content
    d["pray_date"] = i.pray_date
    d["url"] = i.url
    return JsonResponse(d)


def monthlypray(request, day=None):
    if day is None:
        day = date.today().strftime("%Y-%m-%d")
    return JsonResponse(MonthlyPray.objects.filter(pray_date=day).first())


def igenaptar_content(request, day=None):
    if day is None:
        day = date.today().strftime("%Y-%m-%d")
    i = Igenaptar.objects.filter(pray_date=day).first()

    text = strip_tags(i.content)
    response = ""
    if (text.find(day) == -1):
        return HttpResponse(response)

    # Serach list

    search_list = [day, "OLVASMÁNY", "SZENTLECKE",
                   "VÁLASZOS", "ZSOLTÁR", "† EVANGÉLIUM",
                   "Újratöltés", "ALLELUJA", "EGYETEMES",
                   "KÖNYÖRGÉSEK",
                   "EVANGÉLIUM ELŐTTI VERS",
                   "Kezdőének:",
                   "Könyörgés:",
                   ]
    text_breakpoints = []

    for word in search_list:
        idxList = [m.start() for m in re.finditer(word, text)]
        for idx in idxList:
            text_breakpoints.append({"idx": idx, "word": word})
    text_breakpoints = sorted(text_breakpoints, key=lambda i: i['idx'])
    # text_breakpoints.sort(key=idx)
    print(text_breakpoints)
    # text_breakpoints.filter()
    # response +=
    parsed = dict()

    for key, tbp in enumerate(text_breakpoints):
        if key + 1 < len(text_breakpoints):
            parsed[tbp["word"]] = text[tbp["idx"]+len(tbp["word"]):text_breakpoints[key+1]["idx"]].strip()
        else:
            parsed[tbp["word"]] = text[tbp["idx"]+len(tbp["word"]):].strip()
            # text_breakpoints[]
            # response += "-" * 30 + "<br/>"
    # print(parsed)
    # Save
    i = Igenaptar.objects.filter(pray_date=day).first()
    # if i.title is None:
    i.title = parsed.get(day, "")
    i.szentlecke = parsed.get("SZENTLECKE", "")
    i.zsoltar = parsed.get("ZSOLTÁR", "")
    i.alleluja = parsed.get("ALLELUJA", "")
    i.evangelium = parsed.get("† EVANGÉLIUM", "")
    i.konyorges = parsed.get("KÖNYÖRGÉSEK", "")
    i.save()

    # title
    response += "<br/>" + "-" * 30 + "<br/>"
    response += "TITLE" + "<br/>"
    response += parsed.get(day, "")
    # szentlecke
    response += "<br/>" + "-" * 30 + "<br/>"
    response += "SZENTLECKE" + "<br/>"
    response += parsed.get("SZENTLECKE", "")
    # zsoltar
    response += "<br/>" + "-" * 30 + "<br/>"
    response += "ZSOLTÁR" + "<br/>"
    response += parsed.get("ZSOLTÁR", "")
    # alleluja
    response += "<br/>" + "-" * 30 + "<br/>"
    response += "ALLELUJA" + "<br/>"
    response += parsed.get("ALLELUJA", "")
    # evangelium
    response += "<br/>" + "-" * 30 + "<br/>"
    response += "† EVANGÉLIUM" + "<br/>"
    response += parsed.get("† EVANGÉLIUM", "")
    # konyorges
    response += "<br/>" + "-" * 30 + "<br/>"
    response += "KÖNYÖRGÉSEK" + "<br/>"
    response += parsed.get("KÖNYÖRGÉSEK", "")
    response += "<br/>" + "-" * 30 + "<br/>"
    response += "<br/>" + "-" * 30 + "<br/>"
    response += text
    return HttpResponse(response)
