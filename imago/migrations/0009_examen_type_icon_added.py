# Generated by Django 3.0.6 on 2020-06-10 17:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('imago', '0008_relations_step'),
    ]

    operations = [
        migrations.AddField(
            model_name='examentype',
            name='icon_name',
            field=models.CharField(default='fire', max_length=255),
        ),
        migrations.AddField(
            model_name='examentype',
            name='icon_type',
            field=models.CharField(choices=[('MaterialCommunityIcons', 'MaterialCommunityIcons')], default='MaterialCommunityIcons', max_length=255),
        ),
    ]
