from django.contrib.auth.models import Group, User
from rest_framework import serializers

from .models import DailyPray, Examen, ExamenType, MonthlyPray, ExamenStep, ExamenMusic


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']


class DailyPraySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = DailyPray
        fields = ['id', 'pray_date', 'title', 'content', 'url']


class MonthlyPraySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = MonthlyPray
        fields = ['pray_date', 'title', 'content', 'url']


class ExamenSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Examen
        fields = ['examen_type', 'content']


class ExamenStepSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ExamenStep
        fields = ['examen_type', 'is_examen', 'content', 'ordernum', 'wait_time', ]


class ExamenMusicSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ExamenMusic
        fields = ['examen_type', 'url']


class ExamenTypeSerializer(serializers.HyperlinkedModelSerializer):
    step = ExamenStepSerializer(many=True, read_only=True)
    music = ExamenMusicSerializer(many=True, read_only=True)

    class Meta:
        model = ExamenType
        fields = ['id', 'name', 'info', 'icon_name', 'icon_type', 'step', 'music']
