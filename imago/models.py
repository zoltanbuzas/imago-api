from django.utils.translation import gettext_lazy as _
from django.db import models


class Language(models.TextChoices):
    HU = 'HU', _('Hungarian')


class IconTpye(models.TextChoices):
    MaterialCommunityIcons = 'MaterialCommunityIcons', _('MaterialCommunityIcons')


class Igenaptar(models.Model):
    """" Napi útravaló tartalom
        Tartalom forrása:
        https://igenaptar.katolikus.hu/
    """
    pray_date = models.DateField()
    content = models.TextField(null=True, blank=True)
    lang = models.CharField(max_length=3, choices=Language.choices,
                            default=Language.HU,)
    title = models.TextField(null=True, blank=True)
    szentlecke = models.TextField(null=True, blank=True)
    zsoltar = models.TextField(null=True, blank=True)
    valaszos_zsoltar = models.TextField(null=True, blank=True)
    alleluja = models.TextField(null=True, blank=True)
    evangelium = models.TextField(null=True, blank=True)
    konyorges = models.TextField(null=True, blank=True)
    egyetemes_konyorgesek = models.TextField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified = models.DateTimeField(auto_now=True, null=True, blank=True)


class DailyPray(models.Model):
    """ Napi útravaló
    """
    lang = models.CharField(max_length=3, choices=Language.choices,
                            default=Language.HU,)
    pray_date = models.DateField()
    title = models.TextField(null=True, blank=True)
    content = models.TextField(null=True, blank=True)
    url = models.URLField(name="url", verbose_name="MP3", null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified = models.DateTimeField(auto_now=True, null=True, blank=True)


class MonthlyPray(models.Model):
    """ Havi útravaló
    """
    lang = models.CharField(max_length=3, choices=Language.choices,
                            default=Language.HU,)
    pray_date = models.DateField()
    title = models.TextField(null=True, blank=True)
    content = models.TextField(null=True, blank=True)
    url = models.URLField(name="url", verbose_name="MP3", null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified = models.DateTimeField(auto_now=True, null=True, blank=True)

    def __str__(obj):
        return f"{obj.title} {obj.pray_date.year}-{obj.pray_date.month}"


class ExamenType(models.Model):
    """ Examen Type
    """
    id = models.CharField(max_length=50, primary_key=True, unique=True)
    name = models.CharField(max_length=255)
    info = models.TextField(null=True, blank=True)
    icon_name = models.CharField(max_length=255, default="fire")
    icon_type = models.CharField(max_length=255, choices=IconTpye.choices, default=IconTpye.MaterialCommunityIcons)
    created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified = models.DateTimeField(auto_now=True, null=True, blank=True)
    ordernum = models.IntegerField(default=0)

    def __str__(obj):
        return f"{obj.name} {obj.info}"

    class Meta:
        verbose_name = "Examen Type"
        ordering = ["ordernum"]


class Examen(models.Model):
    """ Examen
    """
    lang = models.CharField(max_length=3, choices=Language.choices,
                            default=Language.HU,)
    examen_type = models.ForeignKey('ExamenType', related_name="questions", on_delete=models.CASCADE)
    content = models.TextField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified = models.DateTimeField(auto_now=True, null=True, blank=True)


class ExamenStep(models.Model):
    """ Examen
    """
    lang = models.CharField(max_length=3, choices=Language.choices,
                            default=Language.HU,)
    examen_type = models.ForeignKey('ExamenType', related_name="step", on_delete=models.CASCADE)
    is_examen = models.BooleanField(default=False)
    content = models.TextField(null=True, blank=True)
    ordernum = models.PositiveIntegerField(default=0)
    wait_time = models.PositiveIntegerField(default=10)
    created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        verbose_name = "Examen Step"
        ordering = ["ordernum"]


class ExamenMusic(models.Model):
    """ Examen
    """
    lang = models.CharField(max_length=3, choices=Language.choices,
                            default=Language.HU,)
    examen_type = models.ForeignKey('ExamenType', related_name="music", on_delete=models.CASCADE)
    url = models.URLField(name="url", verbose_name="MP3", null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        verbose_name = "Examen Music"
