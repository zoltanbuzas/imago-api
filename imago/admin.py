from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from rangefilter.filter import DateRangeFilter

from .models import Examen, ExamenType, Igenaptar, DailyPray, MonthlyPray, ExamenStep, ExamenMusic


@admin.register(Examen)
class ExamenAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'modified')
    fieldsets = (
        (None, {'fields': (
                'examen_type', 'content')
                }),
        (_('Language'), {'fields': ('lang', )}),
    )

    list_display = ('pk', 'examen_type', 'content')
    search_fields = ('examen_type', 'lang', 'content',)
    list_filter = ('examen_type', 'lang',)


class ExamenStepInline(admin.TabularInline):
    model = ExamenStep
    extra = 0


class ExamenMusicInline(admin.TabularInline):
    model = ExamenMusic
    extra = 0


@admin.register(ExamenType)
class ExamenTypeAdmin(admin.ModelAdmin):
    inlines = [ExamenStepInline, ExamenMusicInline, ]
    readonly_fields = ('created', 'modified',)
    fieldsets = (
        (None, {'fields': (
                'name', 'info',)
                }),
        (_('Icon'), {'fields': ('icon_type', 'icon_name',)}),
        (_('Dates'), {'fields': ('created', 'modified',)}),
    )

    list_display = ('pk', 'name', 'info', 'ordernum', 'created')
    search_fields = ('pk', 'name', 'info',)
    list_filter = ('name', 'info', 'ordernum',)


@admin.register(DailyPray)
class DailyPrayAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'modified',)
    fieldsets = (
        (None, {'fields': (
                'pray_date', 'title', 'content', 'url')
                }),
        (_('Language'), {'fields': ('lang', )}),
        (_('Dates'), {'fields': ('created', 'modified',)}),
    )

    list_display = ('pk', 'pray_date', 'title', 'url', 'created',)
    search_fields = ('pk', 'pray_date', 'title', 'content',)
    list_filter = (
        ('pray_date', DateRangeFilter),
    )


@admin.register(MonthlyPray)
class MonthlyPrayAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'modified',)
    fieldsets = (
        (None, {'fields': (
                'pray_date', 'title', 'content', 'url')
                }),
        (_('Language'), {'fields': ('lang', )}),
        (_('Dates'), {'fields': ('created', 'modified',)}),
    )

    list_display = ('pk', 'pray_date', 'title', 'url', 'created',)
    search_fields = ('pk', 'pray_date', 'title', 'content',)


@admin.register(Igenaptar)
class IgenaptarAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'modified',)
    fieldsets = (
        (None, {'fields': (
                'pray_date', 'content',)
                }),
        (_('Details'), {'fields': ('title', 'szentlecke', 'zsoltar',
                                   'valaszos_zsoltar', 'alleluja',
                                   'evangelium', 'konyorges',
                                   'egyetemes_konyorgesek', )}),
        (_('Language'), {'fields': ('lang', )}),
        (_('Dates'), {'fields': ('created', 'modified',)}),
    )

    list_display = ('pk', 'pray_date', 'title')
    search_fields = ('pk', 'pray_date', 'title', 'content',)
